﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;



namespace ShopWorkerNameSpace
{

    public class ShopWorker
    {
        public GoodWorker Goods;


        SqlConnection Connection;


        //string str_conn = "Data Source=1-311-DEN;Initial Catalog=shop;Integrated Security=True";
        string str_conn = "Data Source=ADMIN-ПК;Initial Catalog=shop;Integrated Security=True";
        

        public ShopWorker()
        {
            Connection = new SqlConnection();
            Goods = new GoodWorker(Connection);
        }



        public void ConnectShop()
        {
            Connection.ConnectionString = str_conn;
            Connection.Open();
        }


        public void CloseConnections()
        {
            if (Connection != null)
            {
                Connection.Close();
            }
        }



        public DataTable SelectSuppliers(int id_good)
        {

            DataTable table = new DataTable();
            SqlDataAdapter Adapter = new SqlDataAdapter();
            Adapter.SelectCommand = new SqlCommand();
            Adapter.SelectCommand.Connection = Connection;

            try
            {
                Adapter.SelectCommand.CommandText =
                    "select * from Поставщик inner join Товары_Поставщики on Поставщик.id = Товары_Поставщики.id_p where id_t="
                    + id_good.ToString();


                Adapter.Fill(table);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при отборе поставщиков!");
                return null;
            }

            return table;
        }


        public int ExecProc(string good_name)
        {
            int num = 0;

            try
            {
                SqlCommand command = new SqlCommand();
                command.CommandType = CommandType.StoredProcedure;
                command.Connection = Connection;
                command.CommandText = "proc2_";
                command.Parameters.Add("@tname", SqlDbType.NVarChar);
                command.Parameters["@tname"].Value = good_name;
                command.Parameters["@tname"].Direction = ParameterDirection.Input;

                command.Parameters.Add("@num", SqlDbType.Int);
                command.Parameters["@num"].Direction = ParameterDirection.Output;

                command.ExecuteNonQuery();
                num = System.Convert.ToInt32(command.Parameters["@num"].Value);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при выполнении хранимой процедуры!");
            }

            return num;
        }


    }






}
