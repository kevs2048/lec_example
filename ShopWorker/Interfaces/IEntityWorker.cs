﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;


namespace ShopWorkerNameSpace
{
    public interface IEntityWorker<T>
    {
        DataTable GetAll();
        void Delete(T obj);
        void Update(T obj);
        void Add(T obj);
    }
}
