﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Data;
using System.Data.SqlClient;


namespace ShopWorkerNameSpace
{
    public class GoodWorker : IEntityWorker<Good>
    {
        
        SqlDataAdapter Adapter;
        SqlConnection Connection;
        

        public GoodWorker(SqlConnection _connect)
        {
            Connection = _connect;
            Adapter = new SqlDataAdapter();

        }
        

        public DataTable GetAll()
        {
            DataTable table = new DataTable();
            Adapter.SelectCommand = new SqlCommand("select * from Товар", Connection);
            Adapter.Fill(table);
            return table;
        }


        public void Update(Good good)
        {
            SqlCommand Command = new SqlCommand();
            Command.Connection = Connection;
            Command.CommandText = "update Товар set Наименование='" + good.name
                + "', " + "Цена=" + good.price
                + ", " + "Количество=" + good.quantity
                + " " + "where id=" + good.id;
            Command.Connection = Connection;
            Command.ExecuteNonQuery();
        }


        public void Delete(Good good)
        {
            // Удаление товара 
        }


        public void Add(Good good)
        {
            // Добавление товара 
        }


    }
}
