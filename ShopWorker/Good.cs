﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopWorkerNameSpace
{
    public class Good
    {

        public int id = 0;
        public string name = "";
        public int quantity = 0;
        public int price = 0;

        public Good(string _name, int _price, int _quantity, int _id)
        {
            this.id = _id;
            this.name = _name;
            this.price = _price;
            this.quantity = _quantity;
        }
        
    }
}
