﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.Tools.Word;
using Microsoft.Office.Tools.Excel;

using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;


using System.Data;
using ShopWorkerNameSpace;


namespace ShopLogicNameSpace
{
    public class ShopLogic
    {

        ShopWorker Shop;
        public SettingsWorker Settings;

        public ShopLogic()
        {
            Shop = new ShopWorker();
            Settings = new SettingsWorker();
        }


        public void StartWork()
        {
            Shop.CloseConnections();
            Shop.ConnectShop();
            //Settings.Load();
        }



        public void FinishWork()
        {
            Shop.CloseConnections();
            //Settings.Save();
        }


        public DataTable GetAllGoods()
        {
            return Shop.Goods.GetAll();
        
        }


        public void UpdateGood()
        {
            Shop.Goods.Update(new Good(GoodData.name, GoodData.price, GoodData.quantity, GoodData.id));
        }




        public DataTable SelectSuppliers(int id_good)
        {
            return Shop.SelectSuppliers(id_good);
        }


        public string TestProc(string good_name)
        {
            return Shop.ExecProc(good_name).ToString();
        }




        public void ReportWord(string DocNameIn, string DocNameOut)
        {
            
            Word.Application WordApp = new Word.Application();
            WordApp.Visible = true;

            Word.Document doc = WordApp.Documents.Open(DocNameIn);

            Word.Bookmark bm2 = doc.Bookmarks["bm2"];

            WordApp.Selection.SetRange(bm2.Range.Start, bm2.Range.End);
            WordApp.Selection.Font.Size = 24;
            WordApp.Selection.Text = "Вставлено из c#";

            doc.SaveAs2(DocNameOut);
            doc.Close();

            WordApp.Quit(Word.WdSaveOptions.wdDoNotSaveChanges);
        
        }



        public void ReportExcel(string DocNameIn, string SheetName, string DocNameOut)
        {
            Excel.Application ExcelApp = new Excel.Application();
            ExcelApp.Visible = true;

            Excel.Workbook book = ExcelApp.Workbooks.Open(DocNameIn);
            Excel.Worksheet sheet = book.Worksheets[SheetName];

            Excel.Range range_sheet = sheet.UsedRange;
            Excel.Range range_cur = range_sheet.Cells[1, 1];

            string test = System.Convert.ToString(range_cur.Value2);

            range_sheet.Cells[1, 0].Value = "Записано из c#";
            
            book.SaveAs(DocNameOut);

            ExcelApp.Quit();
        }

    }

    public static class GoodData
    {
        public static int id = 0;
        public static string name = "";
        public static int quantity = 0;
        public static int price = 0;
    }






}
