﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Serialization;


namespace ShopLogicNameSpace
{
    public class SettingsFieldsClass
    {
        public int Id;
        public string filter_string;
    }



    public class SettingsWorker
    {
        public SettingsFieldsClass SettingsFields;
        public string Filename;

        public SettingsWorker()
        {

            string settins_dir = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\lec_example\\";
            if (Directory.Exists(settins_dir) == false)
            {
                Directory.CreateDirectory(settins_dir);
            }

            Filename = settins_dir + "settings.xml";
            SettingsFields = new SettingsFieldsClass();
        }


        public void Load()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsFieldsClass));
            TextReader reader = new StreamReader(Filename);
            SettingsFields = (SettingsFieldsClass)serializer.Deserialize(reader);
            reader.Close();

        }


        public void Save()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(SettingsFieldsClass));
            TextWriter writer = new StreamWriter(Filename);
            serializer.Serialize(writer, SettingsFields);
            writer.Close();
        }





    }
}
