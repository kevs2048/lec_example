﻿namespace lec_3_example
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.bt_good_edit = new System.Windows.Forms.Button();
            this.tv_master = new System.Windows.Forms.TreeView();
            this.bt_filter = new System.Windows.Forms.Button();
            this.tb_filter = new System.Windows.Forms.TextBox();
            this.bt_report = new System.Windows.Forms.Button();
            this.bt_excel = new System.Windows.Forms.Button();
            this.bt_proc_test = new System.Windows.Forms.Button();
            this.tb_proc_result = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(426, 330);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Соединиться с БД";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(198, 34);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(581, 269);
            this.dataGridView1.TabIndex = 1;
            // 
            // bt_good_edit
            // 
            this.bt_good_edit.Location = new System.Drawing.Point(609, 330);
            this.bt_good_edit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.bt_good_edit.Name = "bt_good_edit";
            this.bt_good_edit.Size = new System.Drawing.Size(170, 30);
            this.bt_good_edit.TabIndex = 2;
            this.bt_good_edit.Text = "Редактировать";
            this.bt_good_edit.UseVisualStyleBackColor = true;
            this.bt_good_edit.Click += new System.EventHandler(this.bt_good_edit_Click);
            // 
            // tv_master
            // 
            this.tv_master.Location = new System.Drawing.Point(23, 34);
            this.tv_master.Name = "tv_master";
            this.tv_master.Size = new System.Drawing.Size(159, 167);
            this.tv_master.TabIndex = 3;
            this.tv_master.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.tv_master_AfterCheck);
            // 
            // bt_filter
            // 
            this.bt_filter.Location = new System.Drawing.Point(23, 273);
            this.bt_filter.Name = "bt_filter";
            this.bt_filter.Size = new System.Drawing.Size(159, 30);
            this.bt_filter.TabIndex = 4;
            this.bt_filter.Text = "Отфильтровать";
            this.bt_filter.UseVisualStyleBackColor = true;
            this.bt_filter.Click += new System.EventHandler(this.bt_filter_Click);
            // 
            // tb_filter
            // 
            this.tb_filter.Location = new System.Drawing.Point(23, 223);
            this.tb_filter.Name = "tb_filter";
            this.tb_filter.Size = new System.Drawing.Size(159, 26);
            this.tb_filter.TabIndex = 5;
            // 
            // bt_report
            // 
            this.bt_report.Location = new System.Drawing.Point(198, 330);
            this.bt_report.Name = "bt_report";
            this.bt_report.Size = new System.Drawing.Size(165, 30);
            this.bt_report.TabIndex = 6;
            this.bt_report.Text = "Отчёт Word";
            this.bt_report.UseVisualStyleBackColor = true;
            this.bt_report.Click += new System.EventHandler(this.bt_report_Click);
            // 
            // bt_excel
            // 
            this.bt_excel.Location = new System.Drawing.Point(198, 366);
            this.bt_excel.Name = "bt_excel";
            this.bt_excel.Size = new System.Drawing.Size(165, 30);
            this.bt_excel.TabIndex = 7;
            this.bt_excel.Text = "Отчёт Excel";
            this.bt_excel.UseVisualStyleBackColor = true;
            this.bt_excel.Click += new System.EventHandler(this.bt_excel_Click);
            // 
            // bt_proc_test
            // 
            this.bt_proc_test.Location = new System.Drawing.Point(198, 421);
            this.bt_proc_test.Name = "bt_proc_test";
            this.bt_proc_test.Size = new System.Drawing.Size(208, 30);
            this.bt_proc_test.TabIndex = 8;
            this.bt_proc_test.Text = "Хранимая процедура";
            this.bt_proc_test.UseVisualStyleBackColor = true;
            this.bt_proc_test.Click += new System.EventHandler(this.bt_proc_test_Click);
            // 
            // tb_proc_result
            // 
            this.tb_proc_result.Location = new System.Drawing.Point(426, 423);
            this.tb_proc_result.Name = "tb_proc_result";
            this.tb_proc_result.Size = new System.Drawing.Size(121, 26);
            this.tb_proc_result.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 481);
            this.Controls.Add(this.tb_proc_result);
            this.Controls.Add(this.bt_proc_test);
            this.Controls.Add(this.bt_excel);
            this.Controls.Add(this.bt_report);
            this.Controls.Add(this.tb_filter);
            this.Controls.Add(this.bt_filter);
            this.Controls.Add(this.tv_master);
            this.Controls.Add(this.bt_good_edit);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button bt_good_edit;
        private System.Windows.Forms.TreeView tv_master;
        private System.Windows.Forms.Button bt_filter;
        private System.Windows.Forms.TextBox tb_filter;
        private System.Windows.Forms.Button bt_report;
        private System.Windows.Forms.Button bt_excel;
        private System.Windows.Forms.Button bt_proc_test;
        private System.Windows.Forms.TextBox tb_proc_result;
    }
}

