﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Data.SqlClient;


using ShopLogicNameSpace;


namespace lec_3_example
{
    public partial class Form1 : Form
    {
        //ShopWorkerNameSpace.ShopWorker shop;
        ShopLogic ShopLg;
        
        GoodEdit GoodEditForm;
        BindingSource binding_filt;
        

        public Form1()
        {
            InitializeComponent();
            //shop = new ShopWorkerNameSpace.ShopWorker();
            ShopLg = new ShopLogic();
            
            ShopLg.Settings.Load();
            tb_filter.Text = ShopLg.Settings.SettingsFields.filter_string;

            dataGridView1.ReadOnly = true;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.MultiSelect = false;

            binding_filt = new BindingSource();

        }
        

        private void tv_master_AfterCheck(object sender, TreeViewEventArgs e)
        {
            foreach (TreeNode node in tv_master.Nodes[0].Nodes)
            {
                if (node.Checked == true)
                {
                    dataGridView1.Columns[node.Name].Visible = true;
                }
                else 
                {
                    dataGridView1.Columns[node.Name].Visible = false;
                }
            }
        }



        void tvMasterInit(string table_name)
        {
            tv_master.Nodes.Clear();
            tv_master.CheckBoxes = true;
            tv_master.Nodes.Add(table_name, table_name);


            string name_column = "";
            for (int i = 0; i < dataGridView1.Columns.Count; i++)
            {
                name_column = dataGridView1.Columns[i].Name;
                tv_master.Nodes[table_name].Nodes.Add(name_column, name_column);
                tv_master.Nodes[table_name].Nodes[name_column].Checked = true;
            
            }

            tv_master.ExpandAll();
        }



        private void button1_Click(object sender, EventArgs e)
        {
            ////shop.ConnectShop(); 
            ShopLg.StartWork();
            
            ////binding_filt.DataSource = shop.ShowGoods();

            //binding_filt.DataSource = ShopLg.GetAllGoods();
            //dataGridView1.DataSource = binding_filt;
            //dataGridView1.Refresh();

            showDataInMainGrid(ShopLg.GetAllGoods());
            
            tvMasterInit("Товар");
        }



        private void showDataInMainGrid(DataTable dt)
        {
            string filter = tb_filter.Text;
            filter = "Наименование like '%" + filter + "%'";

            binding_filt.DataSource = dt;
            dataGridView1.DataSource = binding_filt;
            binding_filt.Filter = filter;
            dataGridView1.Refresh();
        }



        private void bt_good_edit_Click(object sender, EventArgs e)
        {
            
            int id = dataGridView1.CurrentRow.Index;
            GoodData.id =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[0].Value);

            GoodData.name =
                System.Convert.ToString(
                    dataGridView1.Rows[id].Cells[1].Value);

            GoodData.quantity =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[2].Value);

            GoodData.price =
                System.Convert.ToInt32(
                    dataGridView1.Rows[id].Cells[3].Value);

            GoodEditForm = new GoodEdit(ShopLg);
            GoodEditForm.ShowDialog();

            ShopLg.UpdateGood();

            //binding_filt.DataSource = ShopLg.GetAllGoods();
            //dataGridView1.Refresh();

            showDataInMainGrid(ShopLg.GetAllGoods());


        }



        private void bt_filter_Click(object sender, EventArgs e)
        {
            showDataInMainGrid(ShopLg.GetAllGoods());
        }



        private void bt_report_Click(object sender, EventArgs e)
        {
            string DocNameIn = @"c:\Чек.docx";
            string DocNameOut = @"c:\Чек2.docx";
            ShopLg.ReportWord(DocNameIn, DocNameOut);
        }



        private void bt_excel_Click(object sender, EventArgs e)
        {
            string DocNameIn = @"c:\Чек.xlsx";
            string DocNameOut = @"c:\Чек2.xlsx";
            string SheetName = "Лист1";
            ShopLg.ReportExcel(DocNameIn, SheetName, DocNameOut);
        }

        private void bt_proc_test_Click(object sender, EventArgs e)
        {
            tb_proc_result.Text = ShopLg.TestProc("Молоко");
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            ShopLg.Settings.SettingsFields.filter_string = tb_filter.Text;
            ShopLg.Settings.Save();
        }

    }







}
