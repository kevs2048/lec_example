﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Text.RegularExpressions;


using ShopLogicNameSpace;



namespace lec_3_example
{
    public partial class GoodEdit : Form
    {
        //ShopWorkerNameSpace.ShopWorker shop;
        ShopLogicNameSpace.ShopLogic ShopLg;

        public GoodEdit(ShopLogicNameSpace.ShopLogic _shop_lg)
        {
            InitializeComponent();

            ShopLg = _shop_lg;

            this.tb_good_name.Text = GoodData.name;
            this.tb_good_quantity.Text 
                = GoodData.quantity.ToString();
            this.tb_good_price.Text
                = GoodData.price.ToString();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            int price = GetPrice();
            int quantity = getQuatity();
            string name = getName();


            if ((price != -1) && (quantity != -1) && (name != ""))
            {
                GoodData.price = price;
                GoodData.name = name;
                GoodData.quantity = quantity;
                this.Close();
            }



        }


        private int GetPrice()
        {
            int price = -1;
            try
            {
                price = System.Convert.ToInt32
                                        (tb_good_price.Text);

                if (price < 1)
                {
                    throw new Exception("Цена не может быть меньше 1!");
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, 
                    "Ошибка при вводе значения цены!");
                price = -1;
            }

            return price;
        }


        public string getName()
        {
            string name = "";

            if (Regex.Matches(this.tb_good_name.Text, "[0123456789]+").Count == 0)
            {
                name = this.tb_good_name.Text;
            }
            else
            {
                name = "";
            }
            
            return name;

        }


        public int getQuatity()
        {
            int value = -1;
            try
            {
                value = System.Convert.ToInt32(tb_good_quantity.Text);
                if (value < 0)
                {
                    throw new Exception("Количество единиц товара не может быть меньше 0!");               
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка при вводе количества товара");
                value = -1;
            }

            return value;
        }

        private void bt_show_suppliers_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ShopLg.SelectSuppliers(GoodData.id);



        }





    }
}
